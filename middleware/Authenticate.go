package middleware

import (
	"net/http"
	"startupfundinggolang/app/constants"
	"startupfundinggolang/app/controllers/auth"
	"startupfundinggolang/database"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func Authenticate(c *gin.Context) {
	// return func() {
	authHeader := c.GetHeader("Authorization")

	if !strings.Contains(authHeader, "Bearer") {
		response := constants.ResponseFormatter("User Unautorized", http.StatusUnauthorized, "error", nil)
		c.AbortWithStatusJSON(http.StatusUnauthorized, response)

		return
	}

	tokenString := ""
	arrayToken := strings.Split(authHeader, " ")

	if len(arrayToken) == 2 {
		tokenString = arrayToken[1]
	}

	token, err := auth.ValidateTokenJwt(tokenString)
	if err != nil {
		response := constants.ResponseFormatter("Failed Authorization", http.StatusUnauthorized, "error", nil)
		c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		return
	}

	claim, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		response := constants.ResponseFormatter("Invalid Authorization", http.StatusUnauthorized, "error", nil)
		c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		return
	}

	// "id" is user identity id from JWT token
	id := int(claim["id"].(float64))

	db, err := database.ConnectToDatabase()

	table := database.CallUserRepository(db)

	user, err := table.UserFindById(id)

	if err != nil {
		response := constants.ResponseFormatter("Unautorized", http.StatusUnauthorized, "error", nil)
		c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		return
	}
	c.Set("Auth", user)
	// }
}
