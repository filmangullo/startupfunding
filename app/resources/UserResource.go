package resources

import "startupfundinggolang/models"

type UserResource struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Occupation  string `json:"occupation"`
	Email       string `json:"email"`
	ImageURL    string `json:"image_url"`
	AccessToken string `json:"accessToken"`
}

func FormatUserResource(user models.User, token string) UserResource {
	formatter := UserResource{
		ID:          user.ID,
		Name:        user.Name,
		Occupation:  user.Occupation,
		Email:       user.Email,
		ImageURL:    user.AvatarFileName,
		AccessToken: token,
	}

	return formatter
}
