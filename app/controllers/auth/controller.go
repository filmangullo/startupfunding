package auth

import (
	"net/http"
	"startupfundinggolang/app/constants"
	"startupfundinggolang/app/resources"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

type RegisterRequest struct {
	Name       string `json:"name" binding:"required"`
	Occupation string `json:"occupation" binding:"required"`
	Email      string `json:"email" binding:"required,email"`
	Password   string `json:"password" binding:"required"`
}

type LoginRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

func Register(c *gin.Context) {
	var input RegisterRequest

	err := c.ShouldBindJSON(&input)

	if err != nil {
		errors := constants.FormatValidationError(err)

		errorMessage := gin.H{"errors": errors}

		response := constants.ResponseFormatter("Register request failed.", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	user, err := RegisterService(input)

	if err != nil {
		// Check if the error is of type validator.ValidationErrors
		if validationErr, ok := err.(validator.ValidationErrors); ok {
			// Handle validation errors
			errors := constants.FormatValidationError(validationErr)
			errorMessage := gin.H{"errors": errors}
			response := constants.ResponseFormatter("Register failed.", http.StatusUnprocessableEntity, "error", errorMessage)
			c.JSON(http.StatusUnprocessableEntity, response)
			return
		}

		// Handle other types of errors
		errorMessage := gin.H{"errors": []string{err.Error()}}
		response := constants.ResponseFormatter("Register failed.", http.StatusInternalServerError, "error", errorMessage)
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	accessToken, err := GenerateTokenJwt(user.ID)

	if err != nil {
		errors := constants.FormatValidationError(err)

		errorMessage := gin.H{"errors": errors}

		response := constants.ResponseFormatter("Login failed to make access.", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	formatter := resources.FormatUserResource(user, accessToken)

	response := constants.ResponseFormatter("Login is success", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}

func Login(c *gin.Context) {
	var input LoginRequest

	err := c.ShouldBindJSON(&input)

	if err != nil {
		errors := constants.FormatValidationError(err)

		errorMessage := gin.H{"errors": errors}

		response := constants.ResponseFormatter("Login failed.", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	user, err := LoginService(input)

	if err != nil {
		// Check if the error is of type validator.ValidationErrors
		if validationErr, ok := err.(validator.ValidationErrors); ok {
			// Handle validation errors
			errors := constants.FormatValidationError(validationErr)
			errorMessage := gin.H{"errors": errors}
			response := constants.ResponseFormatter("Login failed.", http.StatusUnprocessableEntity, "error", errorMessage)
			c.JSON(http.StatusUnprocessableEntity, response)
			return
		}

		// Handle other types of errors
		errorMessage := gin.H{"errors": []string{err.Error()}}
		response := constants.ResponseFormatter("Login failed.", http.StatusInternalServerError, "error", errorMessage)
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	accessToken, err := GenerateTokenJwt(user.ID)

	if err != nil {
		errors := constants.FormatValidationError(err)

		errorMessage := gin.H{"errors": errors}

		response := constants.ResponseFormatter("Login failed to make access.", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	formatter := resources.FormatUserResource(user, accessToken)

	response := constants.ResponseFormatter("Login is success", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}
