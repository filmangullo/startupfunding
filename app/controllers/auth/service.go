package auth

import (
	"errors"
	"startupfundinggolang/database"
	"startupfundinggolang/models"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

var SecretKey = []byte("STARTUPFUNDING_g0l4ng_KEY")

func RegisterService(input RegisterRequest) (models.User, error) {
	db, err := database.ConnectToDatabase()
	if err != nil {
		return models.User{}, err
	}

	table := database.CallUserRepository(db)

	user, err := table.UserFindByEmail(input.Email)
	if err != nil {
		return models.User{}, err
	}

	// Check if user with the provided email already exists
	if user.ID != 0 {
		return user, errors.New("Email already exists")
	}

	// Createe new user
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
	if err != nil {
		return models.User{}, err
	}

	request := models.User{
		Name:         input.Name,
		Email:        input.Email,
		Occupation:   input.Occupation,
		PasswordHash: string(passwordHash),
		Role:         "user",
	}

	user, err = table.UserCreate(request)
	if err != nil {
		return user, err
	}

	return user, nil
}

func LoginService(input LoginRequest) (models.User, error) {
	db, err := database.ConnectToDatabase()
	if err != nil {
		return models.User{}, err
	}

	table := database.CallUserRepository(db)

	user, err := table.UserFindByEmail(input.Email)
	if err != nil {
		return user, err
	}

	if user.ID == 0 {
		return user, errors.New("User does not exist")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(input.Password))
	if err != nil {
		return user, err
	}

	return user, nil
}

func GenerateTokenJwt(userID int) (string, error) {
	claim := jwt.MapClaims{}
	claim["id"] = userID

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)

	accessToken, err := token.SignedString(SecretKey)
	if err != nil {
		return accessToken, err
	}

	return accessToken, nil
}

func ValidateTokenJwt(token string) (*jwt.Token, error) {
	accessToken, err := jwt.Parse(token, func(accessToken *jwt.Token) (interface{}, error) {
		_, ok := accessToken.Method.(*jwt.SigningMethodHMAC)

		if !ok {
			return nil, errors.New("Invalid signing method")
		}

		return []byte(SecretKey), nil
	})

	if err != nil {
		return accessToken, err
	}
	return accessToken, nil
}
