package usercontroller

import (
	"errors"
	"startupfundinggolang/database"
	"startupfundinggolang/models"
)

func ProfileService(ID int) (models.User, error) {
	db, err := database.ConnectToDatabase()
	if err != nil {
		return models.User{}, err
	}

	table := database.CallUserRepository(db)

	user, err := table.UserFindById(ID)
	if err != nil {
		return models.User{}, err
	}

	// Check if user with the provided email already exists
	if user.ID == 0 {
		return user, errors.New("User undefined")
	}

	return user, nil
}

func AvatarService(ID int, fileLocation string) (models.User, error) {
	db, err := database.ConnectToDatabase()
	if err != nil {
		return models.User{}, err
	}

	table := database.CallUserRepository(db)

	user, err := table.UserFindById(ID)

	user.AvatarFileName = fileLocation
	if err != nil {
		return models.User{}, err
	}

	update, err := table.UserUpdate(user)

	if err != nil {
		return update, err
	}

	return update, nil
}
