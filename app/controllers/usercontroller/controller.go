package usercontroller

import (
	"fmt"
	"net/http"
	"startupfundinggolang/app/constants"
	"startupfundinggolang/app/resources"
	"startupfundinggolang/models"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

func Profile(c *gin.Context) {
	auth := c.MustGet("Auth").(models.User)
	id := auth.ID

	user, err := ProfileService(id)

	if err != nil {
		// Check if the error is of type validator.ValidationErrors
		if validationErr, ok := err.(validator.ValidationErrors); ok {
			// Handle validation errors
			errors := constants.FormatValidationError(validationErr)
			errorMessage := gin.H{"errors": errors}
			response := constants.ResponseFormatter("Login failed.", http.StatusUnprocessableEntity, "error", errorMessage)
			c.JSON(http.StatusUnprocessableEntity, response)
			return
		}

		// Handle other types of errors
		errorMessage := gin.H{"errors": []string{err.Error()}}
		response := constants.ResponseFormatter("Login failed.", http.StatusInternalServerError, "error", errorMessage)
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	formatter := resources.FormatUserResource(user, "")

	response := constants.ResponseFormatter("Login is success", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}

func Avatar(c *gin.Context) {
	file, err := c.FormFile("avatar")

	if err != nil {
		data := gin.H{"is_uploaded": false}
		response := constants.ResponseFormatter("Filed to upload avatar", http.StatusBadRequest, "error", data)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	auth := c.MustGet("Auth").(models.User)
	id := auth.ID

	path := fmt.Sprintf("storage/img/%d-%s", id, file.Filename)

	err = c.SaveUploadedFile(file, path)

	_, err = AvatarService(id, path)

	if err != nil {
		data := gin.H{"is_uploaded": false}
		response := constants.ResponseFormatter("Filed to upload image avatar", http.StatusBadRequest, "error", data)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	data := gin.H{"is_uploaded": true}
	response := constants.ResponseFormatter("Avatar uploaded", http.StatusOK, "success", data)

	c.JSON(http.StatusOK, response)
}
