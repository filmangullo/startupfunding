package initializers

import (
	"startupfundinggolang/app/controllers/auth"
	"startupfundinggolang/app/controllers/usercontroller"
	"startupfundinggolang/middleware"

	"github.com/gin-gonic/gin"
)

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application.
| so exact way of how its working is that you can
| - Api called -> Controller
| - Controller called -> Services
| - Services does the required things and also calls -> Database Connection like
|   fetch/update/delete etc dan gives the rresults.
|
*/
func Api() *gin.Engine {
	// Create a Gin router instance
	router := gin.Default()

	// Gin with the prefix versioning "V1"
	api := router.Group("api/v1")

	// Define your API routes here
	api.POST("/auth/register", auth.Register)
	api.POST("/auth/login", auth.Login)
	api.GET("/user/profile", middleware.Authenticate, usercontroller.Profile)
	api.POST("/user/avatar", middleware.Authenticate, usercontroller.Avatar)

	// Return the router instance
	return router
}
