package database

import (
	"startupfundinggolang/models"

	"gorm.io/gorm"
)

type DatabaseTableUser interface {
	UserCreate(user models.User) (models.User, error)
	UserFindByEmail(user models.User) (models.User, error)
	UserFindById(user models.User) (models.User, error)
}

type userRepository struct {
	db *gorm.DB
}

func CallUserRepository(db *gorm.DB) *userRepository {
	return &userRepository{db}
}

func (r *userRepository) UserCreate(user models.User) (models.User, error) {
	err := r.db.Create(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) UserFindById(ID int) (models.User, error) {
	var user models.User
	err := r.db.Where("id = ?", ID).Find(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) UserFindByEmail(email string) (models.User, error) {
	var user models.User
	err := r.db.Where("email = ?", email).Find(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) UserUpdate(user models.User) (models.User, error) {
	err := r.db.Save(&user).Error

	if err != nil {
		return user, err
	}

	return user, nil
}
